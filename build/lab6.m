clear all
close all

r_1 = read_data("sat_1.txt",6);
r_2 = read_data("sat_2.txt",6);
r_3 = read_data("sat_3.txt",6);

%part1
figure("Name","Evolution central sattelite");
plot3(r_1(:,1),r_1(:,2),r_1(:,3),'-r.');
figure("Name","Evolution normal sattelite");
plot3(r_2(:,1),r_2(:,2),r_2(:,3),'-b.');
figure("Name","Evolution anormal sattelite");
plot3(r_3(:,1),r_3(:,2),r_3(:,3),'-m.');

t = [1e3:60:1e5-60];

%part2 central with normal
figure("Name","Assembling x_c, x_n");
hold on 
plot(t,r_1(:,1),'-r.');
plot(t,r_2(:,1),'-y.');

figure("Name","Assembling y_c, y_n");
hold on 
plot(t,r_1(:,2),'-r.');
plot(t,r_2(:,2),'-y.');

figure("Name","Assembling z_c, z_n");
hold on 
plot(t,r_1(:,3),'-r.');
plot(t,r_2(:,3),'-y.');

%part2 central with anormal
figure("Name","Assembling x_c, x_a");
hold on 
plot(t,r_1(:,1),'-r.');
plot(t,r_3(:,1),'-y.');

figure("Name","Assembling y_c, y_a");
hold on 
plot(t,r_1(:,2),'-r.');
plot(t,r_3(:,2),'-y.');

figure("Name","Assembling z_c, z_a");
hold on 
plot(t,r_1(:,3),'-r.');
plot(t,r_3(:,3),'-y.');

%part2 normal with anormal
figure("Name","Assembling x_n, x_a");
hold on 
plot(t,r_2(:,1),'-r.');
plot(t,r_3(:,1),'-y.');

figure("Name","Assembling y_n, y_a");
hold on 
plot(t,r_2(:,2),'-r.');
plot(t,r_3(:,2),'-y.');

figure("Name","Assembling z_n, z_a");
hold on 
plot(t,r_2(:,3),'-r.');
plot(t,r_3(:,3),'-y.');

%part3 centrak with normal
figure("Name","Discrepancy x_c, x_n");
plot(t,r_1(:,1)-r_2(:,1),'-r.');

figure("Name","Discrepancy y_c, y_n");
plot(t,r_1(:,2)-r_2(:,2),'-r.');

figure("Name","Discrepancy z_c, z_n");
plot(t,r_1(:,3)-r_2(:,3),'-r.');

%part3 centrak with anormal
figure("Name","Discrepancy x_c, x_a");
plot(t,r_1(:,1)-r_3(:,1),'-r.');

figure("Name","Discrepancy y_c, y_a");
plot(t,r_1(:,2)-r_3(:,2),'-r.');

figure("Name","Discrepancy z_c, z_a");
plot(t,r_1(:,3)-r_3(:,3),'-r.');

%part3 normal with anormal
figure("Name","Discrepancy x_a, x_n");
plot(t,r_2(:,1)-r_3(:,1),'-r.');

figure("Name","Discrepancy y_a, y_n");
plot(t,r_2(:,2)-r_3(:,2),'-r.');

figure("Name","Discrepancy z_a, z_n");
plot(t,r_2(:,3)-r_3(:,3),'-r.');
