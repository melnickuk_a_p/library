﻿#pragma once

#include "quartenion.hpp"
#include "funcm.hpp"
#include <fstream>
#include <math.h>
#include <memory>

class gravity_t {
public:
	gravity_t() = default;

	virtual Vector<long double> get_acceleration(const Vector<long double>& X) = 0;
};

class earth_gravity_t : public gravity_t {
protected:
	const double μ = 398600.4418e9;
	const double ae = 6378.136e3;
public:
	earth_gravity_t() : gravity_t() {};
};

class central_gravity_t :public earth_gravity_t {
public:
	central_gravity_t() : earth_gravity_t() {};

	Vector<long double> get_acceleration(const Vector<long double>& X) override {
		return -μ * X / pow(X.length(), 3);
	}
};

class normal_gravity_t : public earth_gravity_t {
protected:
	Vector<long double> C{ { -1082.62575e-6, 2.37089e-6, -6.08e-9, 1.40e-11 } };
public:
	normal_gravity_t() : earth_gravity_t() {
	
		for (uint8_t count = 2u; count < 10u; count+=2u) {
			C.at(count/2-1) = C.at(count/2-1) / sqrt(2 * count + 1.0l);
		}
	
	};

	Vector<long double> get_acceleration(const Vector<long double>& X) override {
		long double r_0 = sqrt(X.at(0) * X.at(0) + X.at(1) * X.at(1));

		long double ρ = X.length();
		long double φ = atan2(X.at(2), r_0);

		long double sum_ρ{};
		long double sum_φ{};
		for (uint8_t n = 2u; n < 10u; n+=2u) {

			long double legandre = Legendre(φ,n,0);
			long double norm_legandre = sqrt(1.0l/2.0l*n*(n+1))*Legendre(φ,n,1);
			
			sum_ρ += (n + 1) * pow(ae / ρ, n)*C.at(n/2 -1)*legandre;
			sum_φ += pow(ae / ρ, n) * C.at(n / 2 - 1 ) * norm_legandre;
		}
		
		long double g_ρ = -μ / pow(ρ, 2) - μ / pow(ρ, 2) * sum_ρ;
		long double g_φ = μ / pow(ρ, 2) * sum_φ;
		long double g_λ = 0;

		if (r_0 == 0.0)
			return Vector<long double>({ 0,0,g_ρ * X.at(2) / ρ });

		Matrix<long double> M(3, 3, {
			X.at(0) / ρ,	 -X.at(0) * X.at(2) / (ρ * r_0),	 -X.at(1) / r_0,
			X.at(1) / ρ,	 -X.at(1) * X.at(2) / (ρ * r_0),	 X.at(0) / r_0,
			X.at(2) / ρ,	 r_0 / ρ,							 0
			}
		);

		return M*Vector<long double>({g_ρ, g_φ, g_λ});
	}
};

class anomal_gravity_t :public normal_gravity_t {
protected:
	const int N = 60;
	std::vector<long double> ε;
	std::vector<Vector<long double>> r_i;
public:
	anomal_gravity_t() : normal_gravity_t() {
		ε.resize(N);
		r_i.resize(N);

		std::ifstream file("data.txt");

		if (file.is_open()) {
			int count = 0;
			double mass{}, x{}, y{}, z{};

			while (file >> mass >> x >> y >> z) {
				ε.at(count) = (mass * 1e-10);
				r_i.at(count) = (Vector<long double>({ x*1e3, y*1e3, z*1e3 }));
				++count;
			}
		}
		file.close();
	}

	Vector<long double> get_acceleration(const Vector<long double>& X) override {
		auto g_n = normal_gravity_t::get_acceleration(X);
		Vector<long double> g_a(3);
		for (uint8_t count = 0u; count < N; ++count) {
			auto r = X - r_i.at(count);
			g_a = g_a + ε.at(count) * r / pow(r.length(), 3);
		}
	
		g_a = -μ * g_a;
		return g_n + g_a;
	}
};