﻿#pragma once

#include "model.hpp"
#include "gravity.hpp"

class sattelite_t : public model_t {
private:
	std::shared_ptr<gravity_t> gm;
public:
	sattelite_t(const Vector<long double>& X, double t0, double t1, double inc, std::shared_ptr<gravity_t> grav_model) : model_t(X, t0, t1, inc), gm(grav_model) {};

	Vector<long double> get_right(const Vector<long double>& X,long double t) const override {
		using namespace physic_const;

		Vector<long double> dX(X.dimension());

		dX.at(0) = X.at(3);
		dX.at(1) = X.at(4);
		dX.at(2) = X.at(5);

		auto g = gm->get_acceleration(Vector<long double>({X.at(0), X.at(1), X.at(2)}));

		dX.at(3) = g.at(0);
		dX.at(4) = g.at(1);
		dX.at(5) = g.at(2);

		return dX;
	};
};
