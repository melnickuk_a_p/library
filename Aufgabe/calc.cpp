﻿#include <iostream>
#include <math.h>

int main() {

	std::cout << std::fixed;
	std::cout.precision(16);

	const double a = 6'378'136.5;
	const double ρ = 1e7;
	for (uint8_t n = 2u; n < 10u; n += 2u)
		std::cout << pow(a / ρ, n) << '\n';

	return 0;
}

