﻿#include <iostream>
#include <string.h>
#include "src/satellite.hpp"
#include "src/constants.hpp"
#include "src/model.hpp"
#include "src/integrator.hpp"

int test() {

	std::cout << "Run test" << std::endl;

	model_t mod(Vector<long double>({ 0.994l, 0, 0, -2.00158510637908252240537862224l }), 0, 100, 0.1);
	DormandPrinceIntegrator integrator(1e-19l);
	integrator.run(mod);
	std::cout << mod.get_result().rows() << '\n';
	mod.load_res2file("res.txt");

	return 0;
}

int quat_test() {

	using namespace math_const;

	std::cout << "Run quat_test" << std::endl;

	Vector<double> vec({ 2,0,0 });
	Quartenion quat(π/ 2.0, Vector<double>({ 0,3,0 }));

	std::cout << vec.rotateByRodrigFormula(π / 2.0, Vector<double>({ 0,3,0 })) <<'\n';
	std::cout << vec.rotateByQuartenion(quat) << '\n';

	return 0;
}

int test_mat() {

	std::cout << "Run mat_test" << std::endl;

	Matrix<double> mat(2,2, {1,0,0,0});
	std::cout << (!mat) * mat << '\n';
	
	return 0;
}

int test_earth() {

	Vector<long double> efemerid_earth({ -2.682050155281264e10, 1.329337796793504e11, 5.766006735947870e10, -2.982095623757431e4, -4.849462152567819e3, -2.101935693278452e3 });
	
	earth_move_model mod(efemerid_earth, 2459945.5*86400.0, 2460117.5 *86400.0, 86400.0);
	DormandPrinceIntegrator integrator(1e-19l);
	integrator.run(mod);
	
	size_t size = mod.get_result().rows();

	std::cout << mod.get_result().get_rows(size-1) << '\n';
	mod.load_res2file("res.txt");

	return 0;
}

int sundial_test() {

	sundial_model model(rad(-90), rad(0), get_JDN(2023, 12, 22, 0, 0, 0));
	DormandPrinceIntegrator integrator(1e-16l);
	integrator.run(model);
	model.load_res2file("res.txt");

	return 0;
}

int blag_test() {
	blag_time_model model;
	DormandPrinceIntegrator integrator(1e-16l);
	integrator.run(model);
	model.load_res2file("res.txt");

	return 0;
}

int grav_test(double e, double a, double i, double Ω, double ω, double ν) {
	std::cout << std::fixed;
	std::cout.precision(16);

	using namespace math_const;
	using namespace physic_const;

	/*Matrix<long double> leg(9, 2);
	Matrix<long double> leg_my(9, 2);
	leg(0, 0) = 1;
	leg(0, 1) = 0;
	leg(1, 0) = 0;
	leg(1, 1) = sqrt(3.0);

	for (double n = 2; n < 9; ++n) {
		for (double m = 0; m < 2; ++m) {
			if (n == m && n != 0) {
				leg(n, m) = leg(n - 1, m - 1) * 1 * sqrt((2 * n + 1) / (2 * n));
			}
			else if (n > m) {
				leg(n, m) = -leg(n - 2, m) * sqrt((((n - 1) * (n - 1) - m * m) * (2 * n + 1)) / ((n * n - m * m) * (2 * n - 3)));
			}else if (n<m)
				leg(n, m) = 0.0;
		}
	}

	for (double n = 0; n < 9; ++n) {
		for (double m = 0; m < 2; ++m) {
			leg_my(n, m) = Legendre(0, n, m);
		}
	}

	std::cout << leg + (- leg_my) << '\n';*/

	double p = a * (1 - e * e);

	//get r
	double r = a * (1 - e * e) / (1 + e * cos(ν));
	double u = ω + ν;

	double x = r * (cos(u) * cos(Ω) - sin(u) * sin(Ω) * cos(i));
	double y = r * (cos(u) * sin(Ω) + sin(u) * cos(Ω) * cos(i));
	double z = r * sin(u) * sin(i);

	double vr = sqrt(μ / p) * e * sin(ν);
	double vτ = sqrt(μ / p) * (1 + e * cos(ν));

	double dx = vr * x / r - vτ * (sin(u) * cos(Ω) + cos(u) * sin(Ω) * cos(i));
	double dy = vr * y / r - vτ * (sin(u) * sin(Ω) - cos(u) * cos(Ω) * cos(i));
	double dz = vr * z / r + vτ * cos(u) * cos(i);

	Vector<long double> R({ x,y,z,dx,dy,dz });

	Vector<long double> test_coord({ 1e7, 0, 0});

	auto grav_model_central = std::make_shared<central_gravity_t>();
	auto g_c = grav_model_central->get_acceleration(test_coord);
	std::cout << g_c << '\n';

	auto grav_model_normal = std::make_shared<normal_gravity_t>();
	auto g_n = grav_model_normal->get_acceleration(test_coord);
	std::cout << g_n << '\n';

	auto grav_model_anomal = std::make_shared<anomal_gravity_t>();
	auto g_a = grav_model_anomal->get_acceleration(test_coord);
	std::cout << g_a << '\n';
	
	sattelite_t sat_tol1(R, 1e3, 1e5, 60.0, grav_model_central);
	sattelite_t sat_tol2(R, 1e3, 1e5, 60.0, grav_model_normal);
	sattelite_t sat_tol3(R, 1e3, 1e5, 60.0, grav_model_anomal);
	DormandPrinceIntegrator integrator(1e-16l);
	integrator.run(sat_tol1);
	integrator.run(sat_tol2);
	integrator.run(sat_tol3);

	sat_tol1.load_res2file("sat_1.txt");
	sat_tol2.load_res2file("sat_2.txt");
	sat_tol3.load_res2file("sat_3.txt");

	return 0;
}

int main(int argn, char* argc[]) {

	if (argn != 2)
		return -1;

	if (strcmp(argc[1],"--lab1") == 0)
		return test_mat();
	if (strcmp(argc[1], "--lab2") == 0)
		return quat_test();
	if (strcmp(argc[1], "--lab3") == 0)
		return test();
	if (strcmp(argc[1], "--test") == 0)
		return test_earth();
	if (strcmp(argc[1], "--lab5_2") == 0)
		return sundial_test();
	if (strcmp(argc[1], "--lab5_3") == 0)
		return blag_test();
	if (strcmp(argc[1], "--lab6") == 0)
		return grav_test(0.5, 10'000'000, rad(42), rad(87), rad(43), rad(0));

	std::cout << "Nobody" << std::endl;

	return 0;
}